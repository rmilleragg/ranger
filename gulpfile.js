// Core Dependencies

var gulp = require('gulp');

// Gulp Dependencies

// Concat: Creates one large file from many - used in minification tasks.
// Debug: Add this between pipes to see what's happening under the hood.
// Duration: Tells you how long Gulp tasks take. Keep your build tasks fast!
// Filesize: Tells you how large outputted files are. Always good to know.
// Jade: Compiles our HTML templating language.
// JSHint: Ensure puny humans don't write subpar Javascript!
// SASS: Compiles our SASS/SCSS files.
// NgAnnotate: No more passing safety arrays in Angular!
// Notify: Tell us when certain things happen (especially errors!).
// Plumber: Prevents Node pipes from unpiping when an error occurs.
// Rename: Exactly what it says - outputs a file with a new name.
// TemplateCache: Loads our templates directly into Angular's templateCache.
// Uglify: Minifies our Javascript!

var coffee = require('gulp-coffee');
var concat = require('gulp-concat');
var debug = require('gulp-debug');
var duration = require('gulp-duration');
var filesize = require('gulp-filesize');
var jade = require('gulp-jade');
var ngAnnotate = require('gulp-ng-annotate');
var notify = require("gulp-notify");
var sass = require('gulp-sass');
var plumber = require("gulp-plumber");
var rename = require('gulp-rename');
var shell = require('gulp-shell');
var templateCache = require('gulp-angular-templatecache');
var uglify = require('gulp-uglify');

// Paths

var paths = {
  coffee: {
    src: [
      'coffee/modules/**/*.coffee',
      'coffee/config/**/*.coffee',
      'coffee/constants/**/*.coffee',
      'coffee/controllers/**/*.coffee',
      'coffee/services/**/*.coffee',
      'coffee/directives/**/*.coffee',
      'coffee/filters/**/*.coffee',
      'coffee/templates/**/*.js',
    ],
    dest: 'www/js',
    watch: 'coffee/**/*.coffee'
  },
  lib: {
    js: {
      src: [
        'lib/idle.js/build/idle.min.js',
        'lib/jquery/dist/jquery.min.js',
        'lib/parse/index.js',
        'lib/firebase/firebase.js',
        'lib/moment/min/moment.min.js',
        'lib/papa-parse/papaparse.min.js',
        'lib/underscore/underscore-min.js',
        'lib/ionic/js/ionic.bundle.js',
        'lib/ngAutocomplete/src/ngAutocomplete.js',
        'lib/angular-moment/angular-moment.js',
        'lib/angularfire/dist/angularfire.min.js',
        'lib/angular-elastic/elastic.js'
      ],
      dest: 'www/js'
    },
    watch: 'lib/**/*'
  },
  sass: {
    src: ['sass/ranger.scss'],
    dest: 'www/css',
    watch: 'sass/**/*.scss'
  },
  templates: {
    src: 'templates/**/*.jade',
    dest: 'www/js',
    watch: 'templates/**/*.jade'
  }
};

// Error Handler
//
// Gulp, by default, fails silently. 
// Plumber enables us to both throw visible errors and keep moving.

function handleError (error) {
  notify.onError({
    title: "Gulp Error", message: error.toString()
  })(error);

  console.log(error.toString());

  this.emit("end");
};

// Minor Tasks: No need to run these unless with special cause.

// Coffee: Compiles our Javascript files and outputs a concatenated, minified file to /www.
// Lib: Compiles all necessary third-party scripts and styles, adding them to /www.
// SASS: Compiles our scss files and outputs a concatenated, minified file to /www.
// Templates: Compiles our templates files and places the templates scripts to /js.
// Watch: Nothing spectacular - just watches your files for changes and re-runs tasks!

gulp.task('lib', function (done) {
  gulp.src(paths.lib.js.src)
    .pipe(plumber({ errorHandler: handleError }))
    .pipe(concat('ranger.concat.js'))
    .pipe(uglify({ preserveComment: false }))
    .pipe(rename('ranger.lib.js'))
    .pipe(filesize())
    .pipe(gulp.dest(paths.lib.js.dest))
    .on('end', done);
});

gulp.task('coffee', ['templates'], function (done) {
  gulp.src(paths.coffee.src)
    .pipe(plumber({ errorHandler: handleError }))
    .pipe(concat('ranger.concat.js'))
    .pipe(coffee())
    .pipe(ngAnnotate())
    //.pipe(uglify())
    .pipe(rename('ranger.js'))
    .pipe(filesize())
    .pipe(gulp.dest(paths.coffee.dest))
    .on('end', done);
});

gulp.task('parse', shell.task([
  'cp -r www/js parse/public',
  'cp -r www/css parse/public',
  'cp -r www/img parse/public',
  'cp -r www/fonts parse/public',
  'cp www/favicon.ico parse/public'
]));

gulp.task('sass', function (done) {
  gulp.src(paths.sass.src)
    .pipe(plumber({ errorHandler: handleError }))
    .pipe(sass())
    .pipe(rename('ranger.css'))
    .pipe(filesize())
    .pipe(gulp.dest(paths.sass.dest))
    .on('end', done);
});

gulp.task('templates', function (done) {
    gulp.src(paths.templates.src)
      .pipe(plumber({ errorHandler: handleError }))
      .pipe(jade())
      .pipe(templateCache({
          filename: 'ranger.templates.js',
          module: 'ranger'
      }))
      .pipe(gulp.dest(paths.templates.dest))
      .on('end', done);
});

gulp.task('watch', function (done) {
  gulp.watch(paths.coffee.watch, ['coffee']);
  gulp.watch(paths.lib.watch, ['lib']);
  gulp.watch(paths.sass.watch, ['sass']);
  gulp.watch(paths.templates.watch, ['coffee']);
});

// Major Tasks: Run 'gulp' for default, or specify a major task name.

// Default: Run this task if you're looking to build once and be done with it!
// Development: Run this task to have gulp rebuild your assets with each change you make.
// Init: At this moment, simply creates the necessary lib file.

gulp.task('default', ['coffee', 'sass']);

gulp.task('development', ['coffee', 'sass', 'watch']);

gulp.task('init', ['lib']);