ranger.directive 'chatBar', ($timeout, userAgentService) ->
	restrict: 'E'
	scope:
		onSend: '='
	link: (scope, element) ->

		scope.focus = ->
			$timeout ->
				$(element.find('input'))[0].focus()
			, 500

		scope.send = (message) ->
			if message == '' then return

			scope.onSend(message);
			scope.message = '';
			
			if !userAgentService.isTabletOrMobileUserAgent()
				scope.focus()

		scope.focus()
	templateUrl: 'partials/chat_bar.html'