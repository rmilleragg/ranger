ranger.directive 'contactItem', ->
	restrict: 'E'
	scope:
		contact: '='
		onClick: '='
	templateUrl: 'partials/item.contact.html'