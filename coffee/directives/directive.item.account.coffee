ranger.directive 'accountItem', ->
	restrict: 'E'
	scope:
		account: '='
		onClick: '='
	templateUrl: 'partials/item.account.html'