# ******************************
# Sidemenu
# ******************************
#
# High-level sidemenu logic.

ranger.directive 'sidemenu',(authenticationService) ->
	restrict: 'E'
	link: (scope) ->
		scope.user = authenticationService.currentUser().toJSON()

		authenticationService.currentCompany().then (company) ->
			scope.company = company.toJSON()

	templateUrl: 'partials/sidemenu.app.html'