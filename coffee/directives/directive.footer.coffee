ranger.directive 'footer', ($interval, $ionicPopup) ->
	restrict: 'E'
	scope:
		footer: '='
	link: (scope) ->
		counter = 0
		ellipsis = '...'

		loading = ->
			scope.ellipsis = ellipsis.slice 0, counter

			if counter > ellipsis.length
			then counter = 0 
			else counter += 1

		$interval loading, 200

		scope.hideFooter = ->
			scope.footer.details = null
			scope.footer.visible = false

		scope.showDetails = ->
			$ionicPopup.alert
				title: 'Error Details'
				template: scope.footer.details

	templateUrl: 'partials/footer.html'

