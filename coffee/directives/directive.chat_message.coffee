ranger.directive 'chatMessage', ($ionicScrollDelegate) ->
	restrict: 'E'
	scope:
		message: '='
	link: (scope, element, attrs) ->
		$ionicScrollDelegate
			.$getByHandle('chat-window')
			.scrollBottom()
	templateUrl: 'partials/chat_message.html'