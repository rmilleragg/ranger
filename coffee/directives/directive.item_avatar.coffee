ranger.directive 'itemAvatar', () ->
	restrict: 'E'
	scope:
		item: '='
	link: (scope) ->
		if !scope.item.image
			scope.letter = (scope.item.name || scope.item.firstName)[0].toUpperCase()
	templateUrl: 'partials/item_avatar.html'