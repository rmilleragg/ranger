# ******************************
# Sidemenu - Active Directive
# ******************************
#
# Template and logic for showing who is online in the sidemenu.

ranger.directive 'sidemenuActiveList', ($interval, $firebase, firebaseService, userService) ->
	restrict: 'E'
	link: (scope) ->

		scope.checkOnline = (user) ->
			scope.usersOnline[user.objectId]

		checkLive = ->
			amOnline = new Firebase("https://burning-heat-5741.firebaseio.com/.info/connected")
			userRef = new Firebase('https://burning-heat-5741.firebaseio.com/live_' + Parse.User.current().get('company').id + '/' + Parse.User.current().id);
			usersOnline = new Firebase('https://burning-heat-5741.firebaseio.com/live_' + Parse.User.current().get('company').id);

			amOnline.on 'value', (snapshot) ->
				if snapshot.val()
					userRef.onDisconnect().set(0)
					userRef.set(1)

			document.onIdle = ->
				userRef.set 2

			document.onAway = ->
				userRef.set 3

			document.onBack = ->
				userRef.set 1

			scope.usersOnline = $firebase(usersOnline).$asObject();
		
		userFindSuccess = (users) ->
			scope.users = userService.parse users
			checkLive()

		userFindError = (error) ->
			console.log(error);

		userService.query()
			.equalTo('company', Parse.User.current().get('company'))
			.find()
			.then userFindSuccess, userFindError
	templateUrl: 'partials/sidemenu.active_list.html'