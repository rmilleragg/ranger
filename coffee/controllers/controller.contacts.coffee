# ******************************
# Contacts Controller
# ******************************

ranger.controller 'contactsController', ($scope, $timeout, $state, $ionicSideMenuDelegate, contactService, csvService) ->

	# Initialize the footer object.
	$scope.footer = {};

	# ----------------------------
	# Item Selection
	# ----------------------------

	# Given that the contact item directive serves more purposes
	# than simple navigation to that contact, we require each
	# controller to tell its contact directives exactly what it
	# wants them to do when selected. This scope method tells 
	# contact directives to navigate to the contact it represents.
	$scope.onContactSelect = (contact) ->
		$state.go 'app.contact', 
			id: contact.objectId

	# ----------------------------
	# Options Menu
	# ----------------------------

	$scope.openOptionsMenu = ->
		ionic.EventController.trigger 'resize'
		$ionicSideMenuDelegate.$getByHandle('contacts-options').toggleRight()

	# ----------------------------
	# Searching Functions
	# ----------------------------	

	# The ion-content directive tends to mess with $scope variables.
	# Attaching our search options to an object ensures reference will
	# carry the day!
	$scope.searchOptions = 
		term: ''
	
	# Search: Updates the Contact list by querying for a specific Contact name.
	$scope.search = search = ->

		# Show the footer message...
		$scope.footer.visible = true

		# Inform the user that the fetch is loading...
		$scope.footer.message = 'Search...'

		searchTerm = $scope.searchOptions.term.replace(/[^a-zA-Z]/g, '').toLowerCase()

		contactService.query()
			.limit(20)
			.ascending 'search'
			.contains 'search', searchTerm
			.find()
			.then onFetchSuccess, onFetchFailure

	# Debounced version of the search function, helps ensure we don't exceed
	# our Parse request limit.
	$scope.onSearch = search = _.debounce search, 250

	# ----------------------------
	# Contact Retrieval
	# ----------------------------

	# OnFetchSuccess: Called when we successfully fetch Contacts.
	onFetchSuccess = (contacts) ->

		# Hide the message footer...
		$scope.footer.visible = false

		# Check that we've actually received contacts.
		# If not, disable paging.
		if contacts.length
			$scope.pagingOptions.enabled = true
			$scope.pagingOptions.paged = contacts.length
		else
			$scope.pagingOptions.enabled = false

		$scope.contacts = contactService.parse contacts
		
		$scope.$apply();

	# OnFetchSuccess: 
	onFetchFailure = ->

		# Show the footer message...
		$scope.footer.visible = true

		# Inform the user that the fetch was unsuccessful...
		$scope.footer.message = 'Server error...'

		# Hide the message bar after a 1000ms delay 
		$timeout -> 
			$scope.footer.visible = false
		, 1000

	# FetchContacts: Fetches all contacts with optional Parse query constraints.
	fetchContacts = ->

		# Clear contacts array, but fill with one element
		# so "No Results!" message isn't triggered prematurely.
		if $scope.contacts then $scope.contacts = [1];

		# Show the footer message...
		$scope.footer.visible = true

		# Inform the user that the fetch is loading...
		$scope.footer.message = 'Loading...'

		# Delegate to the ContactService query() method
		contactService.query()
			.limit(20)
			.ascending 'search'
			.skip 0
			.find()
			.then onFetchSuccess, onFetchFailure

	# ----------------------------
	# Paging Functions
	# ----------------------------

	# Paging options, such as how many contacts we've paged down.
	$scope.pagingOptions =
		enabled: false
		paged: 0

	# OnPagingSuccess: Called when we successfully page additional Contacts.
	onPagingSuccess = (contacts) ->

		# If there are no additional contacts, that means we've paged it all.
		# Disable any future infinite scrolling.
		if !contacts.length then $scope.pagingOptions.enabled = false	

		# Increment the number of paged contacts by the length of contacts received.
		$scope.pagingOptions.paged += contacts.length

		# Add the newly received contacts to $scope.
		$scope.contacts = $scope.contacts.concat contactService.parse contacts

		# Disable the infinite scroll animation.
		$scope.$broadcast 'scroll.infiniteScrollComplete'

		$scope.$apply()
 
 	# OnPagingSuccess: Called when we've unsuccessfully paged additional Contacts.
	onPagingFailure = ->

		# Disable the infinite scroll animation...
		$scope.$broadcast 'scroll.infiniteScrollComplete'

		# Show the footer message...
		$scope.footer.visible = true

		# Inform the user that paging was unsuccessful...
		$scope.footer.message = 'Paging failed...'

		# Hide the message bar after a 1000ms delay 
		$timeout -> 
			$scope.footer.visible = false
		, 1000

	$scope.pageContacts = pageContacts = ->
		if !$scope.pagingOptions.enabled then return

		query = contactService.query()

		if $scope.searchOptions.term.length
			searchTerm = $scope.searchOptions.term.replace(/[^a-zA-Z]/g, '').toLowerCase()

			query.contains 'search', searchTerm

		query
			.ascending 'search'
			.skip $scope.pagingOptions.paged
			.limit(20)
			.find()
			.then onPagingSuccess, onPagingFailure

	# --------------
	# CSV Handling
	# --------------

	$scope.getCSVTemplate = ->
		csvService.getTemplate contactService.fields(), 'contacts_template'

	onCSVParseSuccess = (results) ->
		# Show the message footer, which will keep our users informed...
		$scope.footer.visible = true

		# Trigger any CSV uploading animations...
		$scope.footer.loading = true

		# Inform user that we have begun parsing their data...
		$scope.footer.message = 'Uploading contacts'

		# Take each parse CSV row and run them through the ContactService's
		# Create method. This converts each into a bona-find Contact object.
		contacts = _.map results, (result) -> contactService.create result

		# Take all of the Contact objects and save them to Parse.
		contactService.saveAll contacts
			.then onCSVUploadSuccess, onCSVUploadFailure

	onCSVParseFailure = (error) ->
		
		# Disable any CSV uploading animations...
		$scope.footer.loading = false

		# Upload failed...
		$scope.footer.message = 'Parsing CSV failed...'

		$scope.footer.details = error.message

	# OnCSVUploadFailure: Called when saving of CSV data is unsuccessful.
	onCSVUploadFailure = (error) ->

		# Disable any CSV uploading animations...
		$scope.footer.loading = false

		# Upload failed...
		$scope.footer.message = 'Upload failed...'

		$scope.footer.details = error.message;

	# OnCSVUploadSuccess: Called when saving of CSV data is successful.
	onCSVUploadSuccess = ->
		# Disable any CSV uploading animations...
		$scope.footer.loading = false

		# Inform the user that the upload was successful...
		$scope.footer.message = 'Upload successful!'

		# Hide the message bar after a 1000ms delay 
		$timeout -> 
			$scope.footer.visible = false
		, 1000

		# Show the beautiful, fresh data!
		fetchContacts()

	$scope.uploadCSV = ->
		$ionicSideMenuDelegate.$getByHandle('contacts-options').toggleRight()

		csvService.promptUpload()
			.then onCSVParseSuccess, onCSVParseFailure

	# --------------
	# Initialization
	# --------------

	fetchContacts()
