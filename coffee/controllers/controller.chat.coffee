## Chat Controller - Business logic for our chat view.

ranger.controller 'chatController', ($scope, chatService, authenticationService) ->
	$scope.newMessage = {}

	chatCreationSuccess = (sync) ->
		$scope.messages = sync

	chatCreationFailure = ->
		alert 'Sorry! We couldn\'t post your message...'

	chatService.create(authenticationService.currentUser().get('company').id)
		.then chatCreationSuccess, chatCreationFailure

	$scope.send = (message) ->
		$scope.messages.$add
			text: message
			name: authenticationService.currentUser().toJSON().name,
			image: authenticationService.currentUser().toJSON().image || null

		$scope.newMessage.text = ''