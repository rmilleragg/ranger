# ******************************
# Login Controller
# ******************************

ranger.controller 'introductionController', ($scope, $state) ->
	$scope.user = {}

	submitSuccess = -> 
		$state.go 'app.launchpad'

	submitFailure = (error) ->
		console.log error

	$scope.submit = ->
		if $scope.introductionForm.$valid && $scope.user.password == $scope.user.repeatPassword
			console.log 'Yes!'