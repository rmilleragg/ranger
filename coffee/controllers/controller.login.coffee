# ******************************
# Login Controller
# ******************************

ranger.controller 'loginController', ($scope, $state, $ionicViewService, authenticationService) ->
	$scope.user = {}

	loginSuccess = -> 
		$state.go 'app.launchpad'

	loginFailure = (error) ->
		switch error.code
			when 101 then $scope.message = 'Sorry, those credentials seem to be invalid.'
			else 'Sorry, we couldn\'t log you in.'
		$scope.$apply()

	$scope.login = ->
		if $scope.loginForm.$valid
			authenticationService.login $scope.user.email, $scope.user.password
				.then loginSuccess, loginFailure