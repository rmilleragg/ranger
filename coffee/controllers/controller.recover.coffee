# ******************************
# Login Controller
# ******************************
#
# Business logic for the recover password view.

ranger.controller 'recoverController', ($scope, authenticationService) ->
	$scope.user = {}

	recoverSuccess = ->
		$scope.message = 'Help is on the way!'
		$scope.$apply();

	recoverFailure = ->
		$scope.message = 'Sorry, we couldn\'t reset that account.'
		$scope.$apply();

	$scope.recover = ->
		if $scope.recoverForm.$valid
			authenticationService.recover $scope.user.email
				.then recoverSuccess, recoverFailure