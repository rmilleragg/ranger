ranger.controller 'contactController', (contact, $scope, contactService) ->
	
	# ------------------------------
	# Option Menus
	# ------------------------------	

	$scope.showMainOptions = showMainOptions = ->

	$scope.showSalesCallsOptions = showSalesCallsOptions = ->

	# ----------------------------
	# Initialization
	# ----------------------------

	# Set the $scope Contact, from the raw resolved Contact object.
	$scope.contact = contactService.parse contact

