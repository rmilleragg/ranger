# ******************************
# Invite Controller
# ******************************
#
# Handles acceptance of invites, and displaying error messages if
# validation of them fails.

ranger.controller 'inviteController', ($scope, $state, $stateParams) ->
	
	console.log('invite contronller!');

	if ($stateParams.inviteCode)
		Parse.Cloud.run 'acceptInvite', inviteCode: $stateParams.inviteCode,
			success: ->
				console.log 'Success!'
				$state.go 'app.introduction'
			error: ->
				$scope.showErrorMessage = true
	else
		$scope.showErrorMessage = true;
	