# ******************************
# Launchpad Controller
# ******************************
#
# Controls the launch pad, the first actual view a user sees
# when the app is loaded.

ranger.controller 'launchpadController', ($ionicSideMenuDelegate) ->

	# ------------------------------
	# Initialization
	# ------------------------------

	# Given that this is the first actual 'view',
	# only it can disable sidemenu dragging. It's
	# annoying, but the way Ionic rolls.
	$ionicSideMenuDelegate.canDragContent false