# ******************************
# Ranger Controller
# ******************************
#
# Top-level application controller.

ranger.controller 'rangerController', ($rootScope, $state, $ionicSideMenuDelegate, authenticationService) ->

	# ------------------------------
	# Initialization
	# ------------------------------

	# If the user is authentication, kick off
	# the app, otherwise show them the homepage.
	if authenticationService.currentUser()
		$state.go 'app.launchpad'

	$rootScope.$on '$stateChangeSuccess', ->
		$ionicSideMenuDelegate.toggleRight false
		ionic.EventController.trigger 'resize'