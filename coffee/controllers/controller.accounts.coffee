# ******************************
# Accounts Controller
# ******************************

ranger.controller 'accountsController', ($scope, $timeout, $state, $ionicSideMenuDelegate, accountService, csvService) ->

	# Initialize the footer object.
	$scope.footer = {};

	# ----------------------------
	# Item Selection
	# ----------------------------

	# Given that the account item directive serves more purposes
	# than simple navigation to that account, we require each
	# controller to tell its account directives exactly what it
	# wants them to do when selected. This scope method tells 
	# account directives to navigate to the account it represents.
	$scope.onAccountSelect = (account) ->
		$state.go 'app.account', 
			id: account.objectId

	# ----------------------------
	# Options Menu
	# ----------------------------

	$scope.openOptionsMenu = ->
		$ionicSideMenuDelegate.$getByHandle('accounts-options').toggleRight()
		ionic.EventController.trigger 'resize'

	# ----------------------------
	# Searching Functions
	# ----------------------------	

	# The ion-content directive tends to mess with $scope variables.
	# Attaching our search options to an object ensures reference will
	# carry the day!
	$scope.searchOptions = 
		term: ''
	
	# Search: Updates the Account list by querying for a specific Account name.
	$scope.search = search = ->

		# Show the footer message...
		$scope.footer.visible = true

		# Inform the user that the fetch is loading...
		$scope.footer.message = 'Search...'

		searchTerm = $scope.searchOptions.term.replace(/[^a-zA-Z]/g, '').toLowerCase()

		accountService.query()
			.limit(20)
			.ascending 'search'
			.contains 'search', searchTerm
			.find()
			.then onFetchSuccess, onFetchFailure

	# Debounced version of the search function, helps ensure we don't exceed
	# our Parse request limit.
	$scope.onSearch = search = _.debounce search, 250

	# ----------------------------
	# Account Retrieval
	# ----------------------------

	# OnFetchSuccess: Called when we successfully fetch Accounts.
	onFetchSuccess = (accounts) ->

		# Hide the message footer...
		$scope.footer.visible = false

		# Check that we've actually received accounts.
		# If not, disable paging.
		if accounts.length
			$scope.pagingOptions.enabled = true
			$scope.pagingOptions.paged = accounts.length
		else
			$scope.pagingOptions.enabled = false

		$scope.accounts = accountService.parse accounts
		
		$scope.$apply();

	# OnFetchSuccess: 
	onFetchFailure = ->

		# Show the footer message...
		$scope.footer.visible = true

		# Inform the user that the fetch was unsuccessful...
		$scope.footer.message = 'Server error...'

		# Hide the message bar after a 1000ms delay 
		$timeout -> 
			$scope.footer.visible = false
		, 1000

	# FetchAccounts: Fetches all accounts with optional Parse query constraints.
	fetchAccounts = ->

		# Clear accounts array, but fill with one element
		# so "No Results!" message isn't triggered prematurely.
		if $scope.accounts then $scope.accounts = [1];

		# Show the footer message...
		$scope.footer.visible = true

		# Inform the user that the fetch is loading...
		$scope.footer.message = 'Loading...'

		# Delegate to the AccountService query() method
		accountService.query()
			.limit(20)
			.ascending 'search'
			.skip 0
			.find()
			.then onFetchSuccess, onFetchFailure

	# ----------------------------
	# Paging Functions
	# ----------------------------

	# Paging options, such as how many accounts we've paged down.
	$scope.pagingOptions =
		enabled: false
		paged: 0

	# OnPagingSuccess: Called when we successfully page additional Accounts.
	onPagingSuccess = (accounts) ->

		# If there are no additional accounts, that means we've paged it all.
		# Disable any future infinite scrolling.
		if !accounts.length then $scope.pagingOptions.enabled = false	

		# Increment the number of paged accounts by the length of accounts received.
		$scope.pagingOptions.paged += accounts.length

		# Add the newly received accounts to $scope.
		$scope.accounts = $scope.accounts.concat accountService.parse accounts

		# Disable the infinite scroll animation.
		$scope.$broadcast 'scroll.infiniteScrollComplete'

		$scope.$apply()
 
 	# OnPagingSuccess: Called when we've unsuccessfully paged additional Accounts.
	onPagingFailure = ->

		# Disable the infinite scroll animation...
		$scope.$broadcast 'scroll.infiniteScrollComplete'

		# Show the footer message...
		$scope.footer.visible = true

		# Inform the user that paging was unsuccessful...
		$scope.footer.message = 'Paging failed...'

		# Hide the message bar after a 1000ms delay 
		$timeout -> 
			$scope.footer.visible = false
		, 1000

	$scope.pageAccounts = pageAccounts = ->
		if !$scope.pagingOptions.enabled then return

		query = accountService.query()

		if $scope.searchOptions.term.length
			searchTerm = $scope.searchOptions.term.replace(/[^a-zA-Z]/g, '').toLowerCase()

			query.contains 'search', searchTerm

		query
			.ascending 'search'
			.skip $scope.pagingOptions.paged
			.limit(20)
			.find()
			.then onPagingSuccess, onPagingFailure

	# --------------
	# CSV Handling
	# --------------

	$scope.getCSVTemplate = ->
		csvService.getTemplate accountService.fields(), 'accounts_template'

	onCSVParseSuccess = (results) ->
		# Show the message footer, which will keep our users informed...
		$scope.footer.visible = true

		# Trigger any CSV uploading animations...
		$scope.footer.loading = true

		# Inform user that we have begun parsing their data...
		$scope.footer.message = 'Uploading accounts'

		# Take each parse CSV row and run them through the AccountService's
		# Create method. This converts each into a bona-find Account object.
		accounts = _.map results, (result) -> accountService.create result

		# Take all of the Account objects and save them to Parse.
		accountService.saveAll accounts
			.then onCSVUploadSuccess, onCSVUploadFailure

	onCSVParseFailure = (error) ->
		
		# Disable any CSV uploading animations...
		$scope.footer.loading = false

		# Upload failed...
		$scope.footer.message = 'Parsing CSV failed...'

		$scope.footer.details = error.message

	# OnCSVUploadFailure: Called when saving of CSV data is unsuccessful.
	onCSVUploadFailure = (error) ->

		# Disable any CSV uploading animations...
		$scope.footer.loading = false

		# Upload failed...
		$scope.footer.message = 'Upload failed...'

		$scope.footer.details = error.message;

	# OnCSVUploadSuccess: Called when saving of CSV data is successful.
	onCSVUploadSuccess = ->
		# Disable any CSV uploading animations...
		$scope.footer.loading = false

		# Inform the user that the upload was successful...
		$scope.footer.message = 'Upload successful!'

		# Hide the message bar after a 1000ms delay 
		$timeout -> 
			$scope.footer.visible = false
		, 1000

		# Show the beautiful, fresh data!
		fetchAccounts()

	$scope.uploadCSV = ->
		$ionicSideMenuDelegate.$getByHandle('accounts-options').toggleRight()

		csvService.promptUpload()
			.then onCSVParseSuccess, onCSVParseFailure

	# --------------
	# Initialization
	# --------------

	fetchAccounts()
