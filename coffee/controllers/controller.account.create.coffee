ranger.controller 'createAccountController', ($interval, $scope, $state, $ionicHistory, $ionicSlideBoxDelegate, accountService) ->
	$scope.account = {};

	$interval ->
		$ionicSlideBoxDelegate.enableSlide false
	, 50, 20

	$scope.editDetails = ->
		$ionicSlideBoxDelegate.slide 1

	$scope.editName = ->
		$ionicSlideBoxDelegate.slide 0

	accountCreationSuccess = (account) ->
		$ionicHistory.nextViewOptions 
			disableBack: true

		$state.go 'app.account', id: account.id

	accountCreationFailure = ->
		alert 'Error creating account!'

	$scope.createAccount = ->
		if ($scope.account.name)
			accountService
				.save(accountService.create($scope.account))
				.then accountCreationSuccess, accountCreationFailure
		else
			alert 'Ensure your account has a name!'

		console.log $scope.account