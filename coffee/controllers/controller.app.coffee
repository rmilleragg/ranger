# ******************************
# App Controller
# ******************************
#
# Handles top-level functioning for the core application.

ranger.controller 'appController', ($scope, $state, $window, authenticationService) ->

	# ------------------------------
	# Initialization
	# ------------------------------

	# If the user does not have authentication,
	# show them the homepage.
	if !authenticationService.currentUser()
		$state.go 'lobby.home'

	$scope.isMobile = $window.innerWidth <= 767;

	# ------------------------------
	# Layout Handling
	# ------------------------------

	$scope.toggleFeed = -> 
		$scope.hideFeed = !$scope.hideFeed
		ionic.EventController.trigger 'resize'