# ******************************
# Onboard Controller
# ******************************

ranger.controller 'onboardController', ($scope, $state, authenticationService) ->
	
	# The ion-content creates a new scope,
	# so we obtain ng-model data by referencing
	# an object.
	$scope.details = {}

	$scope.message = 'No credit card required.'

	onboardSuccess = ->
		$scope.onboardSuccess = true
		$scope.$apply()

	onboardFailure = (error) ->
		$scope.message = 'Sorry, there was an error.'
		$scope.$apply()

	$scope.onboard = ->
		if $scope.onboardForm.$valid
			Parse.Cloud.run	'createCompany', $scope.details,
				success: onboardSuccess, 
				error: onboardFailure