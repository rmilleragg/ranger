ranger.controller 'editAccountController', (account, $scope, $state, $ionicHistory, accountService) ->
	$scope.account = accountService.parse account;

	accountEditSuccess = (account) ->
		$ionicHistory.clearCache();
		$ionicHistory.nextViewOptions 
			disableBack: true

		$state.go 'app.account', id: account.id

	accountEditFailure = ->
		alert 'Error creating account!'

	$scope.editAccount = ->
		accountService
			.update($scope.account, account)
			.then accountEditSuccess, accountEditFailure