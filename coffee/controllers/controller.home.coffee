# ******************************
# Home Controller
# ******************************
#
# Controller for our home view.
# This controller is really meant to tackle a bug in 
# Ionic beta-14, where landing at a root view causes
# a preceding view's title to appear in the nav bar.
# We force-hide the nav-header here, and when we've
# posted the issue to the Ionic forums, will include
# a link here.

ranger.controller 'homeController', ($scope) ->

	$scope.hideNavTitle = true
	
	