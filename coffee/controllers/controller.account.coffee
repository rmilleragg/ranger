ranger.controller 'accountController', (account, $scope, $ionicSideMenuDelegate, accountService) ->
	
	# ----------------------------
	# Options Menu
	# ----------------------------

	$scope.openOptionsMenu = ->
		ionic.EventController.trigger 'resize'
		$ionicSideMenuDelegate.$getByHandle('account-options').toggleRight()

	# ----------------------------
	# Initialization
	# ----------------------------

	# Set the $scope Account, from the raw resolved Account object.
	$scope.account = accountService.parse account

