# ******************************
# Ranger Module
# ******************************
#
# Top-level application module.

ranger = angular.module 'ranger', [
	'angularMoment',
	'firebase',
	'ionic',
	'monospaced.elastic',
	'ngAutocomplete'
]

# Module initialization.
#
# 1) Boot Parse.
ranger.run (parseService) -> parseService.init()