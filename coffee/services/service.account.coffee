# ******************************
# Account Service
# ******************************

ranger.service 'accountService', (resourceService) ->

	# The fundamental Account object.
	Account = Parse.Object.extend 'Account'

	# Pointers to be included on queries by default.
	includes = ['creator']

	# Fields: All directly-exposed client-side fields.
	fields = -> [
		'name',
		'email',
		'phone',
		'street',
		'city',
		'state',
		'zip',
		'twitterHandle',
		'facebookHandle',
		'website',
		'notes'
	]

	# Create: Partially-applied version of the resourceService's
	# create method.
	create = _.partial resourceService.create, Account, _, fields()

	# Get: Partially-applied version of the resourceService's
	# get method.
	get = _.partial resourceService.get, Account, _, includes

	# Query: Partially-applied version of the resourceService's
	# query method.
	query = _.partial resourceService.query, Account, includes

	# Parse: Convenience method referenced directly from the
	# resourceService.
	parse = resourceService.parse

	# Save: Convenience method referenced directly from the
	# resourceService.
	save = resourceService.save

	# SaveAll: Convenience method referenced directly from the
	# resourceService.
	saveAll = resourceService.saveAll

	# Update: Takes a raw JSON representation of an object, maps
	# fields to the original object, then saves it.
	update = _.partial resourceService.update, fields()

	{
		create,
		fields,
		get,
		parse,
		query,
		save,
		saveAll,
		update
	}
	