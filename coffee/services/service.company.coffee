# ******************************
# Company Service
# ******************************

ranger.service 'companyService', (resourceService) ->

	# The fundamental Company object.
	Company = Parse.Object.extend 'Company'

	# Pointers to be included on queries by default.
	includes = []

	# Get: Partially-applied version of the resourceService's
	# get method.
	get = _.partial resourceService.get, Company, _, includes

	# Query: Partially-applied version of the resourceService's
	# query method.
	query = _.partial resourceService.query, Company, includes

	# Parse: Convenience method referenced directly from the
	# resourceService.
	parse = resourceService.parse

	# Save: Convenience method referenced directly from the
	# resourceService.
	save = resourceService.save

	# SaveAll: Convenience method referenced directly from the
	# resourceService.
	saveAll = resourceService.saveAll

	{
		get,
		parse,
		query,
		save,
		saveAll
	}