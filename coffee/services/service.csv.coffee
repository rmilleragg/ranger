# ----------------------------
# CSV Service
# ----------------------------
#
# Abstraction for common CSV parsing tasks, all using Papa Parse (http://papaparse.com).

ranger.service 'csvService', ($q) ->

	# CSV Input: Used to prompt users for CSV files.
	csvInput = null

	# GetCSVTemplate: Rather than make our users read some boring walkthrough,
	# we give them a pre-formed CSV template with the correct header ready for them.
	# This function generates that CSV file.
	getTemplate = (fields, fileName) ->

		deferred = $q.defer()

		# Generate the CSV file based on the keys for Account objects.
		# These keys are available via a public method exposed on the AccountService.
		csv = Papa.unparse
			fields: fields

		# Prep the CSV string for URI encoding.
		csv = "data:text/csv;charset=utf-8," + csv

		# This is hack to avoid using $window.open.
		# Allows users to download the template within the same tab.
		#
		# 1) We inject an anchor tag into the DOM.
		link = document.createElement 'a'

		# 2) Make sure the link is hidden.
		link.setAttribute 'style', 'display: none;'

		# 3) Set the anchor tag to the encoded CSV string.
		link.setAttribute 'href', encodeURI csv

		# 4) Set the download attribute to something recognizable.
		link.setAttribute 'download', (fileName || 'template') + '.csv'

		# 5) Use jQuery to trigger the click event on the anchor.
		link.click()

		deferred.resolve()

		deferred.promise

	#OnUpload: Called when user uploads a CSV file for parsing.
	onUpload = ->
		deferred = $q.defer()

		# Delegate to the csvService method
		parse(csvInput[0].files[0])
			.then deferred.resolve, deferred.reject

		deferred.promise	

	# Parse: Convert a FileUpload object into JSON.
	#
	# file: A FileUpload object.
	parse = (file) ->

		# Given we don't know how parseable the data is,
		# let's prepare to return a promise.
		deferred = $q.defer()

		# Papa Parse is the man. http://papaparse.com
		Papa.parse file,
			
			# Called upon a successful parse.
			complete: (results) ->

				# Cache the actual data, discarding metadata
				items = results.data
				
				# Clear any rows contains no values.
				# We use Underscore's reject method to remove
				# any results for which an interating function
				# returns true.
				items = _.reject items, (item) ->

					# Iterating function method above. We use Underscore's
					# every method to check if every value contained in the
					# object is blank. If all values return true (i.e. !value)
					# then _.every will return true, then prompting the reject
					# method to cull out that item.
					_.every item, (value) -> !value

				# Resolve the processed items.
				deferred.resolve items

				# Remove the input from the DOM
				csvInput.remove()

			# Called upon a failed parse.
			error: deferred.reject

			# Ensures the first row in the CSV value we parse 
			# is interpreted as the keys for all subsequent rows. Handy.
			header: true

			# Ensures any row containing no content 
			# (all values are empty strings) will be munged.
			skipEmptyLines: true

		# Return the promise immediately. 
		deferred.promise

	# Upload: Entry point for the CSV uploading process.
	promptUpload = ->

		# Prepare a promise, given we don't know how long the
		# user will take.
		deferred = $q.defer()

		# This process involves creating a hidden file input,
		# then triggering a click on it with jQuery. 
		#
		# 1) We inject an input tag into the DOM.
		csvInput = $ document.createElement 'input'

		# 2) Make sure the link is hidden.
		csvInput.attr 'style', 'display: none;'

		# 3) Ensure the input is of type 'file'
		csvInput.attr 'type', 'file'

		# 4) Bind to the input's change event to file onCSVUpload
		csvInput.change ->
			onUpload()
				.then deferred.resolve, deferred.reject

		# 5) Use jQuery to trigger the click event on the input. 
		csvInput.click()

		# Return the promise immediately. 
		deferred.promise

	{
		getTemplate,
		promptUpload
	}
