# ******************************
# Firebase Service
# ******************************
#
# Firebase powers our real-time functionality, such as chat, who's online, etc...

ranger.service 'firebaseService', ($firebase) ->
	ref = new Firebase("https://burning-heat-5741.firebaseio.com/")

	newRemote = (child) ->
		$firebase(ref.child(child))

	{
		newRemote
	}