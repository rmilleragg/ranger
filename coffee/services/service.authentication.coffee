# ******************************
# Authentication Service
# ******************************
#
# Maintains all state regarding user authentication status.

ranger.service 'authenticationService', (companyService) ->
	
	currentCompanyCache = null;

	currentUser = -> Parse.User.current()

	currentCompany = ->
		if (!currentUser())
			new Parse.Promise.error();
		else if (!currentCompanyCache)
			currentCompanyCache = companyService.get currentUser().get('company').id
		else
			currentCompanyCache

	currentCompany()

	login = (email, password) ->
		Parse.User.logIn email, password

	logout = ->
		Parse.User.logOut()

	recover = (email) ->
		Parse.User.requestPasswordReset(email)

	register = (name, email, password) ->
		user = new Parse.User()

		user.set "name", name
		user.set "username", email
		user.set "password", password
		user.set "email", email

		user.signUp()

	{
		currentUser
		currentCompany
		login
		logout
		recover
		register
	}