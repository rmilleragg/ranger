# ******************************
# User Service
# ******************************

ranger.service 'userService', (resourceService) ->

	# The fundamental User object.
	User = Parse.User

	# Pointers to be included on queries by default.
	includes = ['company']

	# Fields: All directly-exposed client-side fields.
	fields = -> [
		'name',
		'image',
		'thumbnail'
	]

	# Get: Partially-applied version of the resourceService's
	# get method.
	get = _.partial resourceService.get, User, _, includes

	# Query: Partially-applied version of the resourceService's
	# query method.
	query = _.partial resourceService.query, User, includes

	# Parse: Convenience method referenced directly from the
	# resourceService.
	parse = resourceService.parse

	# Save: Convenience method referenced directly from the
	# resourceService.
	save = resourceService.save

	# SaveAll: Convenience method referenced directly from the
	# resourceService.
	saveAll = resourceService.saveAll

	# Update: Takes a raw JSON representation of an object, maps
	# fields to the original object, then saves it.
	update = _.partial resourceService.update, fields()

	{
		fields,
		get,
		parse,
		query,
		save,
		saveAll,
		update
	}
	