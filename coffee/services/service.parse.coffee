# ******************************
# Parse Service
# ******************************
#
# Handles all high-level Parse initialization tasks.

ranger.service 'parseService', (config) ->

	# Init: Lock and load the Parse library.
	init = ->
		Parse.initialize config.parse.appId, config.parse.jsKey

	{
		init 
	}