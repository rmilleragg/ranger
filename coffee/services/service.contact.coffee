# ******************************
# Contact Service
# ******************************

ranger.service 'contactService', (resourceService) ->

	# The fundamental Contact object.
	Contact = Parse.Object.extend 'Contact'

	# Pointers to be included on queries by default.
	includes = ['creator', 'account']

	# Fields: All directly-exposed client-side fields.
	fields = -> [
		'firstName',
		'lastName'
		'title',
		'email',
		'phone',
		'facebookHandle',
		'twitterHandle',
		'linkedinHandle',
		'website',
		'notes'
	]

	# Create: Partially-applied version of the resourceService's
	# create method.
	create = _.partial resourceService.create, Contact, _, fields()

	# Get: Partially-applied version of the resourceService's
	# get method.
	get = _.partial resourceService.get, Contact, _, includes

	# Query: Partially-applied version of the resourceService's
	# query method.
	query = _.partial resourceService.query, Contact, includes

	# Parse: Convenience method referenced directly from the
	# resourceService.
	parse = _.partial resourceService.parse, _, includes

	# Save: Convenience method referenced directly from the
	# resourceService.
	save = resourceService.save

	# SaveAll: Convenience method referenced directly from the
	# resourceService.
	saveAll = resourceService.saveAll

	{
		create,
		fields,
		get,
		parse,
		query,
		save,
		saveAll,
	}
