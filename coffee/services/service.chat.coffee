# ******************************
# Chat Service
# ******************************
#
# Intermediary service for chatroom functionality.

ranger.service 'chatService', ($q, firebaseService) ->
	create = (companyId) ->
		deferred = $q.defer()

		deferred.resolve(firebaseService.newRemote('chat_' + companyId).$asArray());

		deferred.promise

	{
		create
	}