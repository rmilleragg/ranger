# ******************************
# Resource Service
# ******************************

ranger.service 'resourceService', ->

	# Create: Handles creating individual Parse resources.
	create = (ParseObject, resource, fields) ->

		# Initialize a new resource object.
		parseObject = new ParseObject()

		# Add all fields to the new object.
		for key, value of map resource, fields
			parseObject.set key, value

		# Every object is associated with a creating user.
		parseObject.set 'creator', Parse.User.current()

		# Every object is associated with the creating company.
		parseObject.set 'company', Parse.User.current().get 'company'

		# Return the new object.
		parseObject

	# Get: Responsible for retrieving individual resources based on IDs.
	get = (ParseObject, id, includes = []) ->

		# Create a new Parse query.
		resourceQuery = new Parse.Query ParseObject

		for include in includes
			resourceQuery.include include

		# Return the query promise.
		resourceQuery.get id

	includePointers = (resource, includes = []) ->
		jsonResource = resource.toJSON()

		for include in includes
			if resource.get(include)
				jsonResource[include] = resource.get(include).toJSON()

		jsonResource

	# Parse: Return Parse objects as standard JSON objects.
	# Parse (the service) follows several Backbone idioms, which can be a tad frustrating.
	# This function allows us to extract raw JSON from Parse objects when needed.
	parse = (resource, includes) ->

		# Make sure we have something to work with.
		if !resource?
			throw new Error 'Must pass resource(s) for parsing!'

		# If it's not an array of objects (just one), 
		# prepare an empty array for our parsed objects.
		if angular.isArray resource
			parsedResource = _.map resource, (o) -> includePointers o, includes
		else
			parsedResource = includePointers resource, includes

		parsedResource

	# Find: Return all or a specific subset of a company's resources.
	query = (ParseObject, includes = []) ->

		# Create a new Parse query.
		resourceQuery = new Parse.Query ParseObject

		for include in includes
			resourceQuery.include include

		resourceQuery

	# Map: Maps raw resources to their supported keys.
	map = (resource, fields) ->

		# Initialize an empty object to hold keyed values.
		mappedResource = {}
		
		# Take each element in the fields array and map it to the resource.
		for key in fields
			mappedResource[key] = resource[key]

		# Return the mapped object.
		mappedResource

	# Save: Handles saving individual resources.
	save = (resource) ->

		# Returns promise, which is resolved upon a successful save.
		resource.save()

	# SaveAll: Saves multiple Account objects at once.
	saveAll = (resources) ->

		# Returns a promise, which is resolved upon all resources being saved.
		Parse.Object.saveAll resources

	update = (fields, json, original) ->
		_.each json, (value, key) ->
			if (fields.indexOf(key) != -1)
				original.set(key, value)

		original.save()

	# Gotta love destructuring assignment.
	# This returns an ParseObject keyed to the above functions.
	{
		create,
		get,
		parse,
		query,
		save,
		saveAll,
		update
	}