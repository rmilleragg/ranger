# ******************************
# Routing Configuration
# ******************************
#
# Contains all Ranger application routing, while it's feasible.

ranger.config ($stateProvider, $urlRouterProvider) ->

	# ------------------------------
	# Lobby Views
	# ------------------------------
	#
	# Odd name, yes. A 'lobby' view is any view
	# which is not a part of the core view.

	$stateProvider
		.state 'lobby',
			abstract: true
			templateUrl: 'views/lobby.html'
		.state 'lobby.home',
			url: '/'
			templateUrl: 'views/home.html'
		.state 'lobby.about',
			url: '/about'
			templateUrl: 'views/about.html'
		.state 'lobby.login',
			url: '/login'
			controller: 'loginController'
			templateUrl: 'views/login.html'
		.state 'lobby.onboard',
			url: '/onboard'
			controller: 'onboardController'
			templateUrl: 'views/onboard.html'
		.state 'lobby.recover',
			url: '/recover'
			controller: 'recoverController'
			templateUrl: 'views/recover.html'

	# ------------------------------
	# App Views
	# ------------------------------

	$stateProvider
		.state 'app',
			abstract: true
			url: '/app'
			controller: 'appController'
			templateUrl: 'views/app.html'

	$stateProvider
		.state 'app.accounts',
			url: '/accounts'
			controller: 'accountsController'
			templateUrl: 'views/app.accounts.html'
		.state 'app.account',
			url: '/accounts/:id'
			controller: 'accountController'
			resolve: 
				account: ($stateParams, accountService) ->
					accountService.get $stateParams.id
			templateUrl: 'views/app.account.html'
		.state 'app.createAccount',
			url: '/accounts/new'
			controller: 'createAccountController'
			templateUrl: 'views/app.account.create.html'
		.state 'app.editAccount',
			url: '/accounts/:id/edit'
			controller: 'editAccountController'
			resolve: 
				account: ($stateParams, accountService) ->
					accountService.get $stateParams.id
			templateUrl: 'views/app.account.edit.html'

	$stateProvider
		.state 'app.contacts',
			url: '/contacts'
			controller: 'contactsController'
			templateUrl: 'views/app.contacts.html'
		.state 'app.contact',
			url: '/contacts/:id'
			controller: 'contactController'
			resolve: 
				contact: ($stateParams, contactService) ->
					contactService.get $stateParams.id
			templateUrl: 'views/app.contact.html'

	$stateProvider
		.state 'app.launchpad',
			url: '/launchpad'
			controller: 'launchpadController'
			templateUrl: 'views/app.launchpad.html'
		.state 'app.introduction',
			url: '/introduction'
			controller: 'introductionController'
			templateUrl: 'views/app.introduction.html'
		.state 'app.chat',
			url: '/chat'
			controller: 'chatController'
			templateUrl: 'views/app.chat.html'

	$stateProvider
		.state 'invite',
			url: '/invite/:inviteCode'
			controller: 'inviteController'
			templateUrl: 'views/app.introduction.html'

	$urlRouterProvider.otherwise('/');

