# ******************************
# Ionic Configuration
# ******************************
#
# Configure Ionic to our sweet liking.

ranger.config ($ionicConfigProvider) ->

	$ionicConfigProvider.views.transition 'android'

	$ionicConfigProvider.backButton.previousTitleText false

	# iOS does have a nicer back button...
	$ionicConfigProvider.backButton.icon 'ion-ios7-arrow-left'
	$ionicConfigProvider.backButton.text ''