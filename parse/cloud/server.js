var express = require('express');
var parseExpressHttpsRedirect = require('parse-express-https-redirect');

var app = express();

app.set('views', 'cloud/views');
app.set('view engine', 'jade');

app.use(parseExpressHttpsRedirect());

app.get('/', function (req, res) {
	res.render('index.jade');
});

app.use(function(req, res, next){
  res.status(404);

  if (req.accepts('html')) {
    res.render('404', { url: req.url });
    return;
  }

  if (req.accepts('json')) {
    res.send({ error: 'Not found' });
    return;
  }

  res.type('txt').send('Not found');
});

app.listen();