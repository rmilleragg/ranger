// ******************************
// Dependencies
// ******************************

Mandrill = require('mandrill');
Mandrill.initialize('AzUrjP2lVIq6114RynIsBg');

// ******************************
// Initialize RangerCRM.com server...
// ******************************

require('cloud/server.js');

// ******************************
// User Hooks
// ******************************

// ******************************
// Company Hooks
// ******************************

// ******************************
// User Onboarding Functions
// ******************************

Parse.Cloud.define("inviteMember", function(request, response) {
});

require('cloud/functions/createCompany.js').init(Mandrill);

require('cloud/functions/acceptInvite.js').init(Mandrill);
require('cloud/functions/sendInvite.js').init(Mandrill);

Parse.Cloud.beforeSave('Account', function (request, response) {
	
	// Cache the account object from the request.
	var account = request.object;

	// ------------------------------
	// Error Messages
	// ------------------------------

	var missingCompany = 'All accounts must be associated with a company. ' +
		'This is something Ranger should be doing automatically for you, ' +
		'so it\'s no bueno that you\'re seeing this error message.';

	var missingName = 'All accounts must be given a name. ' +
		'If you\'re uploading a CSV, make sure each account ' +
		'has an entry in the "name" column.';

	// ------------------------------
	// Account Validation
	// ------------------------------

	if (!account.get('company')) {
		response.error(missingCompany);
		return;
	}

	if (!account.get('name')) {
		response.error(missingName);
		return;
	}

	// ------------------------------
	// Before-Save Operations
	// ------------------------------

	// See Parse's blog post on implementing search functionality (http://goo.gl/gU87Cn).
	// To ensure searchability, we save a separate column with the fully lowercased name.
	account.set('search', account.get('name').replace(/[^a-zA-Z]/g, '').toLowerCase())

	// Validation passed - all clear!
	response.success()
});

// ******************************
// Contact Handlers
// ******************************

Parse.Cloud.beforeSave('Contact', function (request, response) {
	
	// Cache the contact object from the request.
	var contact = request.object;

	// ------------------------------
	// Error Messages
	// ------------------------------

	var missingCompany = 'All contacts must be associated with a company. ' +
		'This is something Ranger should be doing automatically for you, ' +
		'so it\'s no bueno that you\'re seeing this error message.';

	var missingName = 'All contacts must be given a first name. ' +
		'If you\'re uploading a CSV, make sure each contact ' +
		'has an entry in the "firstName" column.';

	// ------------------------------
	// Contact Validation
	// ------------------------------

	if (!contact.get('company')) {
		response.error(missingCompany);
		return;
	}

	if (!contact.get('firstName')) {
		response.error(missingName);
		return;
	}

	// ------------------------------
	// Before-Save Operations
	// ------------------------------

	// See Parse's blog post on implementing search functionality (http://goo.gl/gU87Cn).
	// To ensure searchability, we save a separate column with the fully lowercased name.
	var search = '';

	// Add the first name at the outset.
	search += contact.get('firstName');

	// If a last name exists, add it.
	if (contact.get('lastName'))
		search += contact.get('lastName');

	// Validation passed - all clear!
	response.success()
});