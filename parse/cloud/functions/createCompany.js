// ******************************
// Create Company
// ******************************
//
// Responsible for the following in sequence:
// 1) Creating a new company.
// 2) Creating a minimal admin user for that company.
// 3) Setting roles and ACLs for the company (admin & normal).
// 4) Assigning normal and admin privileges to admin user.
// 5) Assigning proper read access to the new company.
// 6) Sending the admin user and email with invite link and temp. password.

exports.init = function (Mandrill) {
	Parse.Cloud.define('createCompany', function (request, response) {
		
		// ------------------------------
		// Initialization
		// ------------------------------		

		// If we didn't use the master key, we'd receive permission
		// errors editing the ACLs.
		Parse.Cloud.useMasterKey();

		var userEmail, companyName;

		var Company = Parse.Object.extend('Company');

		var Invitation = Parse.Object.extend('Invitation');

		// InviteCode: Code that admin will redeem when creating his/her account.
		// InvitePassword: Temporary password for the admin, should they exit the
		// onboarding process prematurely
		var inviteCode = Math.random().toString(36).substring(7);
		var invitePassword = Math.random().toString(36).substring(7);

		// ------------------------------
		// Validation
		// ------------------------------

		if (!request.params.userEmail) {
			response.error('When creating a company, you must supply the creator\'s email as userEmail!');
			return;
		}
		else {
			userEmail = request.params.userEmail;
		}

		if (!request.params.companyName) {
			response.error('When creating a company, you must supply the company\'s name as companyName!');
			return;
		}
		else {
			companyName = request.params.companyName;
		}	

		// ------------------------------
		// Core Functions
		// ------------------------------

		// CreateCompany: Creates and saves our new company.
		function createCompany() {
			company = new Company();

			company.set('name', companyName);
			company.set('invites', [inviteCode]);

			return company.save();
		}

		// CreateEmail: Once user, company, and ACLs are saved,
		// we send the admin user an invite email.
		function createEmail() {
			var deferred = new Parse.Promise();

			var inviteMessage = '' +
				'Welcome to Ranger!\n' +
				'\n' +
				'    To get started, simply click this link: ' +
				'https://rangercrm.com/#/invite/' + inviteCode + '.\n' +
				'    If you need to log into your account before you set a new password, ' +
				'your temporary password is: ' + invitePassword + '.\n' +
				'    We keep it simple so you can focus on your customers. If you have a comment ' +
				'or suggestion, write us at hi@rangercrm.com.\n' +
				'\n' +
				'Cheers,\n' +
				'\n' +
				'The Ranger Team';

			Mandrill.sendEmail({
				message: {
					text: inviteMessage,
					subject: 'Your new Ranger company is ready!',
					from_email: 'ryan@rangercrm.com',
					from_name: 'Ranger',
					to: [{
						email: userEmail,
						name: userEmail
					}]
				}
			}, {
				success: function () {
					deferred.resolve();
				},
				error: function () {
					deferred.reject();
				}
			});

			return deferred;
		}

		// CreateInvite: Creates and saves our new company.
		function createInvite(user) {
			invitation = new Invitation();

			invitation.set('user', user);
			invitation.set('company', user.get('company'));
			invitation.set('code', inviteCode);

			return invitation.save();
		}

		// CreateRoles: This function initializes all necessary roles
		// for permissions in the company. We currently observe only two
		// levels: normal and admin. Only admins can:
		// 1) Send invites.
		// 2) Create/edit products.
		function createRoles(invite) {
			var normalACL, adminACL;
			var normalRole, adminRole;

			var company = invite.get('company');

			normalACL = new Parse.ACL();
			adminACL = new Parse.ACL();

			normalACL.setPublicReadAccess(false);
			normalACL.setPublicWriteAccess(false);

			adminACL.setPublicReadAccess(false);
			adminACL.setPublicWriteAccess(false);

			normalRole = new Parse.Role(invite.get('company').id, normalACL);
			adminRole = new Parse.Role(invite.get('company').id + '_admin', adminACL);

			normalRole.getUsers().add(invite.get('user'));
			adminRole.getUsers().add(invite.get('user'));

			return Parse.Promise.when(normalRole.save(), adminRole.save(), company.save());
		}

		// CreateUser: Create the first admin user for the company.
		function createUser(company) {
			user = new Parse.User();

			user.set('email', userEmail);
			user.set('password', invitePassword);
			user.set('username', userEmail);
			user.set('company', company);

			return user.signUp();
		}

		// SetRoles: Assign an ACL to the company, and make the admin role a
		// child of the normal role.
		function setRoles(normalRole, adminRole, company) {
			var companyACL = new Parse.ACL();

			companyACL.setPublicWriteAccess(false);
			companyACL.setPublicReadAccess(false);

			companyACL.setRoleReadAccess(normalRole, true);
			companyACL.setRoleWriteAccess(adminRole, true);

			company.setACL(companyACL);

			normalRole.getRoles().add(adminRole);

			return Parse.Promise.when(normalRole.save(), company.save());
		}

		// ------------------------------
		// Execute
		// ------------------------------

		createCompany()
			.then(createUser)
			.then(createInvite)
			.then(createRoles)
			.then(setRoles)
			.then(createEmail)
			.then(response.success, response.error);
	});
};
