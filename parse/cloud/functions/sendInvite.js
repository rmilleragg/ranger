// ******************************
// Send Invite
// ******************************

exports.init = function (Mandrill) {
	Parse.Cloud.define('sendInvite', function (request, response) {
		// ------------------------------
		// Initialization
		// ------------------------------		

		// If we didn't use the master key, we'd receive permission
		// errors editing the ACLs.
		Parse.Cloud.useMasterKey();

		var authenticatedUser;
		var authenticatedCompany;
		var invitedUser;

		var inviteEmail;

		// Initialize object types required by this module.
		var Invitation = Parse.Object.extend('Invitation');
		var Company = Parse.Object.extend('Company');

		// InviteCode: Code that admin will redeem when creating his/her account.
		// InvitePassword: Temporary password for the admin, should they exit the
		// onboarding process prematurely
		var inviteCode = Math.random().toString(36).substring(7);
		var invitePassword = Math.random().toString(36).substring(7);

		// ------------------------------
		// Validation
		// ------------------------------

		if (!request.user) {
			response.error('Invites may only be sent by authenticated users!');
			return;
		}
		else {
			authenticatedUser = request.user;
		}

		if (!request.params.inviteEmail) {
			response.error('When creating an invite, you must specify an email to send it to.');
			return;
		}
		else {
			inviteEmail = request.params.inviteEmail;
		}

		function createEmail() {
			var deferred = new Parse.Promise();

			Mandrill.sendTemplate({
				template_name: 'invite',
				template_content: [],
				message: {
					subject: 'Join the ' + authenticatedCompany.get('name') + ' team on Ranger!',
					from_email: 'ryan@rangercrm.com',
					from_name: 'Ranger',
					global_merge_vars: [{
						name: 'user',
						content: authenticatedUser.get('name')
					}, {
						name: 'company',
						content: authenticatedCompany.get('name')
					}, {
						name: 'code',
						content: inviteCode
					}, {
						name: 'password',
						content: invitePassword
					}],
					to: [{
						email: inviteEmail,
						name: inviteEmail
					}]
				}
			}, {
				success: function () {
					deferred.resolve();
				},
				error: function (error) {
					deferred.reject(error);
				}
			});

			return deferred;
		}

		function createInvite(user) {
			invitation = new Invitation();

			invitation.set('user', user);
			invitation.set('company', authenticatedUser.get('company'));
			invitation.set('invitedBy', authenticatedUser);
			invitation.set('code', inviteCode);

			return invitation.save();
		}

		function createUser() {
			var deferred = new Parse.Promise();
			var user = new Parse.User();

			user.set('email', inviteEmail);
			user.set('password', invitePassword);
			user.set('username', inviteEmail);
			user.set('company', authenticatedCompany);
			user.set('new', true);

			user
				.signUp()
				.then(function (newUser) {
					invitedUser = newUser;

					deferred.resolve(newUser);
				}, function (error) {
					deferred.reject(error);
				});

			return deferred;
		}

		function getCompany() {
			var deferred = new Parse.Promise();
			var query = new Parse.Query(Company);

			query
				.get(authenticatedUser.get('company').id)
				.then(function (company) {
					authenticatedCompany = company;

					deferred.resolve(company);
				}, function (error) {
					deferred.reject(error);
				});

			return deferred;
		}

		function setRole() {
			var deferred = new Parse.Promise();
			var query = new Parse.Query(Parse.Role);

			query.equalTo("name", authenticatedUser.get('company').id);

			query.first().then(function (role) {
				if (role) {
					role.getUsers().add(invitedUser);
					role.save(null, {
						success: function () {
							deferred.resolve();
						},
						error: function (error) {
							deferred.reject(error);
						}
					});
				}
				else { 
					deferred.reject('Error fetching role for new user.');
				}
			}, function () {
				deferred.reject();
			});

			return deferred;
		}

		function verifyAdmin() {
			var deferred = new Parse.Promise();
			var query = new Parse.Query(Parse.Role);

			query.equalTo("name", authenticatedUser.get('company').id + '_admin');
			query.equalTo("users", authenticatedUser);

			query.first().then(function (role) {
				if (role)
					deferred.resolve(role);
				else 
					deferred.reject('User must have admin privileges to send invite!');
			}, function (error) {
				deferred.reject(error);
			});

			return deferred;
		}

		verifyAdmin()
			.then(getCompany)
			.then(createUser)
			.then(createInvite)
			.then(setRole)
			.then(createEmail)
			.then(response.success, response.error);
	});
};