// ******************************
// Accept Invite
// ******************************

exports.init = function (Mandrill) {
	Parse.Cloud.define('acceptInvite', function (request, response) {
		
		// ------------------------------
		// Initialization
		// ------------------------------		

		// If we didn't use the master key, we'd receive permission
		// errors editing the ACLs.
		Parse.Cloud.useMasterKey();

		// Initialize object types required by this module.
		var Invitation = Parse.Object.extend('Invitation');

		var userInvitation;

		var inviteCode;

		if (!request.params.inviteCode) {
			response.error('An invite code must be provided to the acceptInvite endpoint!');
			return;
		}
		else {
			inviteCode = request.params.inviteCode;
		}

		function acceptInvitation() {
			userInvitation.set('accepted', true);
			userInvitation.set('dateAccepted', new Date());

			return userInvitation.save();
		}

		function getInvitation() {
			var deferred = new Parse.Promise();
			var query = new Parse.Query(Invitation);

			query.equalTo('code', inviteCode);
			query.notEqualTo('accepted', true);

			query.include('invitedBy');
			query.include('user');
			query.include('company');

			query.first().then(function (invitation) {

				console.log(invitation);

				if (invitation) {
					userInvitation = invitation;

					deferred.resolve();
				}
				else {
					deferred.reject('Unable to find invitation!');
				}
			}, function (error) {
				deferred.reject(error);
			});

			return deferred;
		}

		function notifySender() {
			var deferred = new Parse.Promise();

			if (userInvitation.get('invitedBy')) {
				Mandrill.sendTemplate({
					template_name: 'invite-accepted',
					template_content: [],
					message: {
						subject: 'Your invitation has been accepted!',
						from_email: 'ryan@rangercrm.com',
						from_name: 'Ranger',
						global_merge_vars: [{
							name: 'recipient',
							content: userInvitation.get('invitedBy').get('name')
						}, {
							name: 'invitee',
							content: userInvitation.get('user').get('email')
						}, {
							name: 'company',
							content: userInvitation.get('company').get('name')
						}],
						to: [{
							name: userInvitation.get('invitedBy').get('name'),
							email: userInvitation.get('invitedBy').get('email')
						}]
					}
				}, {
					success: function () {
						deferred.resolve();
					},
					error: function (error) {
						deferred.reject(error);
					}
				});
			}
			else {
				deferred.resolve();
			}

			return deferred;			
		}

		getInvitation()
			.then(acceptInvitation)
			.then(authenticateUser)
			.then(notifySender)
			.then(response.success, response.error)
	});
}